import imp
from flask import Flask
import os
from flask_jwt_extended import JWTManager
from src.auth import auth
from src.bookmarks import bookmarks
from src.database import db

def create_app(test_config=None):
    app = Flask(__name__,instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="SECRET_KEY",
        SQLALCHEMY_DATABASE_URI = 'sqlite:///bookmarks.db',
        SQLALCHEMY_TRACK_MODIFICATIONS = False,
        JWT_SECRET_KEY="super-secret"
    )

    

    @app.get("/")
    def home():
        return "Hello World"

    @app.get("/hello")
    def hello():
        return {"name":"Hello World"}

    db.app=app
    db.init_app(app)

    JWTManager(app)

    app.register_blueprint(auth,url_prefix='/api')
    app.register_blueprint(bookmarks)
    return app