from flask import Flask,jsonify


app = Flask(__name__)


@app.get("/")
def home():
    return {"name":"Hello World"}

@app.get("/hello")
def hello():
    return jsonify({"name":"Hello World"})