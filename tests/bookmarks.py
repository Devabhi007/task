from os import access
from werkzeug.security import check_password_hash
import sys
from os.path import dirname
from unittest import TestResult
import json
from flask_jwt_extended import create_access_token

import bookmarks
sys.path.append(dirname(r"C:\Users\user\Downloads\rest_api\src"))
from flask import Flask
from src.database import db, Bookmark
from src import create_app
from flask_security.utils import login_user
from src.app import app
import unittest


class FlaskTest(unittest.TestCase):
    def setUp(self):
        self.app = create_app(test_config="testing")
        self.appctx=self.app.app_context()

        self.appctx.push()

        self.client=self.app.test_client

        db.create_all()


    def tearDown(self):
        self.appctx.pop()



    def test_bookmarkss(self):

        token=create_access_token(identity='testuser')

        headers={
            "Authorization":f"Bearer {token}"
        }

        response=self.client().get('http://127.0.0.1:5000/api/v1/bookmarks/',headers=headers)
        self.assertEqual(response.status_code, 200)
        print(response.data)
        self.assertTrue(b"data" in response.data)


    def test_create_order(self):
        
        token=create_access_token(identity='testuser')

        headers={
            "Authorization":f"Bearer {token}"
        }

        data={
                'url':'https://justharun.wordpress.com/2018/11/29/the-flask-mega-tutorial-part-i-hello-world/',
                "body":"body", 
                "user_id":4
             }
        response=self.client().post('http://127.0.0.1:5000/api/v1/bookmarks/',json=data,headers=headers)

        try:
            self.assertEqual(response.status_code, 201)
        except:
            orders= Bookmark.query.filter_by(user_id=4)

if __name__ == "__main__":
    unittest.main()