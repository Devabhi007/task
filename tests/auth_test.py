from os import access
from werkzeug.security import check_password_hash
import sys
from os.path import dirname
from unittest import TestResult
import json
from flask_jwt_extended import create_access_token

sys.path.append(dirname(r"C:\Users\user\Downloads\rest_api\src"))
from flask import Flask
from src.database import db, User
from src import create_app
from flask_security.utils import login_user
from src.app import app
import unittest


app = Flask(__name__)
from src.bookmarks import bookmarks

app.register_blueprint(bookmarks, url_prefix="")


class FlaskTest(unittest.TestCase):
    # *******************************************register************************************************
    # check for response 200 url('/')
    def setUp(self):
        self.app = create_app(test_config="testing")

        self.appctx = self.app.app_context()

        self.appctx.push()

        self.client = self.app.test_client
        self.data1 = {
            "username": "kkkkishan12",
            "email": "kkkishakn12@gmail.com",
            "password": "abhi@786",
        }
        db.create_all()

    def tearDown(self):
        user = User.query.filter_by(username="kkkkishan12").delete()

        self.appctx.pop()

        # with self.app.app_context():
        #     db.create_all()

    def test_register(self):

        res = self.client().post(
            "http://127.0.0.1:5000/api/register", json=(self.data1)
        )
        try:
            self.assertEqual(res.status_code, 409)
        except:
            self.assertEqual(res.status_code, 201)

    # check if content return is application/json
    def test_register_content(self):
        res = self.client().post(
            "http://127.0.0.1:5000/api/register", json=(self.data1)
        )
        try:
            self.assertEqual(res.status_code, 409)
            self.assertEqual(res.content_type, "application/json")

        except:
            self.assertEqual(res.status_code, 201)
            self.assertEqual(res.content_type, "application/json")

    #    # check for data returned
    def test_register_data(self):
        res = self.client().post(
            "http://127.0.0.1:5000/api/register", json=(self.data1)
        )
        try:
            self.assertEqual(res.status_code, 409)

        except:
            self.assertEqual(res.status_code, 201)
            self.assertTrue(["username", "email"] in res.data)

    # ************************************************login****************************************************
    def test_login(self):

        data = {"email": "abhi1@gmail.com", "password": "aj@123"}
        response = self.client().post("http://127.0.0.1:5000/api/login", json=data)
        self.assertEqual(response.status_code, 200)

    def test_login_content(self):
        data = {"email": "abhi1@gmail.com", "password": "aj@123"}
        response = self.client().post("http://127.0.0.1:5000/api/login", json=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, "application/json")

    def test_login_data(self):
        res = self.client().post("http://127.0.0.1:5000/api/login", json=(self.data1))
        self.assertEqual(res.status_code, 200)
        self.assertTrue(b"user" in res.data)

    def test_get_all_me(self):
        user = User.query.filter_by(email="abhi1@gmail.com").first()

        token = create_access_token(identity=user.id)

        headers = {"Authorization": f"Bearer {token}"}

        response = self.client().get("http://127.0.0.1:5000/api/me", headers=headers)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(b"username" in response.data)


if __name__ == "__main__":
    unittest.main()
