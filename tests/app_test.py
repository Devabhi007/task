import sys
from os.path import dirname
from unittest import TestResult
sys.path.append(dirname("/home/abhishekjaiswal/Desktop/rest_api/src"))


try:
    from src.app import app
    import unittest
except Exception as e:
    print("module not found {}".format(e))

class FlaskTest(unittest.TestCase):
    #check for response 200 url('/')
    def test_home(self):
        tester = app.test_client(self)
        response  = tester.get('/')
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    #check if content return is application/json
    def test_home_content(self):
        tester = app.test_client(self)
        response  = tester.get('/')
        self.assertEqual(response.content_type, "application/json")

    # check for data returned
    def test_home_data(self):
        tester = app.test_client(self)
        response  = tester.get('/')
        self.assertTrue(b"name" in response.data)

    #check for response 200 url('/hello')
    def test_hello(self):
        tester = app.test_client(self)
        response  = tester.get('/hello')
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    #check if content return is application/json
    def test_hello_content(self):
        tester = app.test_client(self)
        response  = tester.get('/hello')
        self.assertEqual(response.content_type, "application/json")

    # check for data returned
    def test_hello_data(self):
        tester = app.test_client(self)
        response  = tester.get('/hello')
        self.assertTrue(b"name" in response.data)

if __name__ == '__main__':
    unittest.main()